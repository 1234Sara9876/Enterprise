﻿using Enterprise.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Enterprise.Controllers
{
    public class HomeController : Controller
    {
        DCEnterpriseDataContext dc = new DCEnterpriseDataContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Sobre()
        {
            return View();
        }

        public ActionResult Servicos()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Servicos(Orcamento or)
        {
            if (ModelState.IsValid)
            {
                dc.Orcamentos.InsertOnSubmit(or);
            }

            try
            {
                dc.SubmitChanges();

                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                return View();
            }
        }

    }
}