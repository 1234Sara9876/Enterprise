﻿using Enterprise.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Enterprise.Controllers
{
    public class NewslettersController : Controller
    {
        DCEnterpriseDataContext dc = new DCEnterpriseDataContext();

        // GET: Newsletters
        public ActionResult Index()
        {
            return View(dc.Newsletters);
        }

        // POST: Newsletters/Create
        [HttpPost]
        public ActionResult News(Newsletter novaNews)
        {
            if (ModelState.IsValid)
            {
                dc.Newsletters.InsertOnSubmit(novaNews);
            }
            try
            {
                dc.SubmitChanges();
                return RedirectToAction("Index","Home");
            }
            catch
            {
                return View();
            }
        }
    }
}
